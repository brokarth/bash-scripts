apt-get install -y aptitude
echo "

-----------------*********Agregando Repositorios*********-----------------

"
apt-add-repository ppa:ninja-ide-developers/daily
add-apt-repository ppa:alecive/antigone
add-apt-repository ppa:jd-team/jdownloader
add-apt-repository ppa:ingalex/super-boot-manager
add-apt-repository ppa:libreoffice/ppa
add-apt-repository ppa:otto-kesselgulasch/gimp
add-apt-repository ppa:gnome3-team/gnome3
add-apt-repository ppa:me-davidsansome/clementine

echo "

-----------------*********Eliminando Software no deseado*********-----------------

"
apt-get remove unity-lens-shopping unity-scope-musicstores ubuntuone-client deja-dup
aptitude purge -y xine rhythmbox totem
mv /usr/bin/bluetooth-applet /usr/bin/bluetooth-applet-old
mv /usr/share/oneconf/oneconf-service /usr/share/oneconf/oneconf-service-old

echo "

-----------------*********Actualizando Sistema*********-----------------

"

aptitude update
aptitude -y full-upgrade

echo "

-----------------*********Instalando Software*********-----------------

"

aptitude -y install geany codeblocks pgadmin3 clementine soundconverter vlc alien wine openjdk-7-jdk build-essential awoken-icon-theme  

aptitude -y install jdownloader super-boot-manager ninja-ide gimp flex byacc chromium-browser chromium-browser-l10n dconf-tools subversion autoconf intltool libtool

aptitude -y install libgtk2.0-dev libsdl1.2-dev libgtk-3-dev libxv-dev libxtst-dev libxxf86vm-dev avidemux dvdrip subtitleeditor openshot planner libgtkglext1

aptitude -y install libgtkglextmm-x11-1.2-0 gnome-system-tools bison rar unrar p7zip-full gns3 wireshark gnome-search-tool cairo-dock gnome-shell preload dkms zsnes:i386 zram

aptitude -y install clipgrab

echo "

-----------------*********Configuraciones Varias*********-----------------

"
cd /etc/xdg/autostart/
sed --in-place 's/NoDisplay=true/NoDisplay=false/g' *.desktop

echo FRAMEBUFFER=y | sudo tee /etc/initramfs-tools/conf.d/splash
update-initramfs -u
update-grub
sed 's/DMZ (White)\b/Terranove/g' /usr/share/icons/default/index.theme
reboot

echo "--------- RECUERDA INSTALAR VBAM Y PCSX -------------------"
