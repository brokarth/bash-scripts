#!/bin/bash
for mkvfile in *.mkv; do
    mkvpropedit "$mkvfile" -e info --delete-attachment mime-type:image/png --delete-attachment mime-type:image/jpeg --delete-attachment mime-type:image/jpg -s title="${mkvfile:5:-4}"
done
